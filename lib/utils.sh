#!/usr/bin/env sh

# XXX: $TERM=dumb in CircleCI
# echo "[DEBUG] term='${TERM}'" >&2
if tty -s && [ "$TERM" != 'dumb' ]; then
  COLORS_FOREGROUND_DEBUG=$(tput setaf 4) # Blue
  # COLORS_BACKGROUND_DEBUG=$(tput setbf 4) # Blue
  COLORS_DEBUG="$COLORS_FOREGROUND_DEBUG"
  COLORS_RESET=$(tput sgr0)
else
  COLORS_FOREGROUND_DEBUG="\e[94m" # Light Blue
  # COLORS_BACKGROUND_DEBUG="\e[44m" # Blue
  COLORS_DEBUG="$COLORS_FOREGROUND_DEBUG"
  # COLORS_DEBUG="$COLORS_BACKGROUND_DEBUG"
  COLORS_RESET="\e[39m"
  # COLORS_RESET="\e[49m"
fi

_debug() {
  printf "${COLORS_DEBUG}[DEBUG] %s${COLORS_RESET}\n" "${1}"
}

# FIXME: detect if sudo is needed (id != 0 + sudoers exists)
_run_as_root() {
  if [ "$(id -u)" != "0" ]; then
    _debug "Executing with privileges ..."
    sudo "$@"
  else "$@"
  fi
}

if [ -n "$PATH_BASE" ]; then
  _debug "Using base: ${PATH_BASE}"
else
  # shellcheck disable=2155
  export PATH_BASE="$(pwd)"
  _debug "Base: ${PATH_BASE}"
fi