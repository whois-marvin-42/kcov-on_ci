#!/usr/bin/env bash
################################################################################
# shellcheck disable=SC1091
. "lib/utils.sh"
################################################################################

# COVERAGE_FILE="${RPATH_COVERAGE}/shunit2.[0-9a-f]*/coverage.json"
COVERAGE_FILE="${RPATH_COVERAGE}/${SCRIPT}.[0-9a-f]*/coverage.json"

################################################################################

rm -rf "$RPATH_COVERAGE"
# "${RPATH_BIN}/coverage.sh"
if [ "$BUILD_KCOV_INSTALL" = 'local' ]; then
  # # "${RPATH_BIN}/kcov" --include-path=bin "${RPATH_COVERAGE}" shunit2 "bin/${SCRIPT}"
  # "${RPATH_BIN}/kcov" --debug=31 --include-path=bin "${RPATH_COVERAGE}" "bin/${SCRIPT}"
  # "${RPATH_BIN}/kcov" --include-path=bin "${RPATH_COVERAGE}" "bin/${SCRIPT}"
  "${RPATH_BIN}/kcov" "${RPATH_COVERAGE}" "bin/${SCRIPT}"
else
  # kcov --debug=31 --include-path=bin "${RPATH_COVERAGE}" "shunit2 bin/${SCRIPT}"
  # kcov --debug=31 --include-path=bin "${RPATH_COVERAGE}" "bin/${SCRIPT}"
  # kcov --include-path=bin "${RPATH_COVERAGE}" "bin/${SCRIPT}"
  kcov "${RPATH_COVERAGE}" "bin/${SCRIPT}"
fi
#-------------------------------------------------------------------------------
_debug "Content of coverage folder (${RPATH_COVERAGE})"
ls -la "${RPATH_COVERAGE}/"
#-------------------------------------------------------------------------------
# shellcheck disable=SC2086
COVERAGE_FILE="$(realpath ${COVERAGE_FILE})"
_debug "Reading coverage from ${COVERAGE_FILE}"
cat "${COVERAGE_FILE}"
COVERED='-.--'
COVERED_REGEX="^\s+\"percent_covered\": \"([[:digit:]]+\.[[:digit:]]+)\","
if COVERED="$(grep -E "$COVERED_REGEX" "$COVERAGE_FILE")"
then [[ "$COVERED" =~ $COVERED_REGEX ]] && COVERED="${BASH_REMATCH[1]}"
fi
echo "[COVER] ${COVERED}%" >&2