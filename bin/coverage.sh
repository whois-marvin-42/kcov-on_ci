#!/usr/bin/env bash

[ -n "$COVERAGE_DIR" ] || COVERAGE_DIR='coverage'

# XXX: just because I'm becoming paranoïd ;)
[ -d "$COVERAGE_DIR" ] && rm -rf "$COVERAGE_DIR"

# XXX: binary provided by distribution is too old
# kcov ${G_DIR_COVERAGE} ./bin/dummy.sh
# XXX: using the last version compiled on the fly
./bin/kcov ${COVERAGE_DIR} ./bin/dummy.sh