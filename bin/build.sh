#!/usr/bin/env sh
################################################################################
# shellcheck disable=SC1091
. "lib/utils.sh"
################################################################################

if cd "$RPATH_BUILD_KCOV"; then
  _debug "Changed directory to build folder for kcov: '${RPATH_BUILD_KCOV}'"
else
  _debug "Couldn't change directory to build folder for kcov: '${RPATH_BUILD_KCOV}'"
  exit 2
fi

_debug "Building kcov from sources (${BUILD_KCOV_INSTALL} install)"
if [ "$BUILD_KCOV_INSTALL" = 'local' ]
then cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/ "${PATH_BASE}/${RPATH_CACHE_KCOV}"
else cmake ..
fi
make
if [ "$BUILD_KCOV_INSTALL" = 'local' ]
then make install DESTDIR="$PATH_BASE"
else _run_as_root make install
fi
#-------------------------------------------------------------------------------
cd "$PATH_BASE" || exit 2
_debug "Showing kcov version"
if [ "$BUILD_KCOV_INSTALL" = 'local' ]
then "${RPATH_BIN}/kcov" --version
else kcov --version
fi