#!/usr/bin/env bash

dummy() {
  echo 'dummy'
}

test_dummy() {
  assertEquals 'dummy message' 'dummy' "$(dummy)"
}

if [ "$(basename "$0")" != 'shunit2' ]; then
  echo "[DEBUG] Executing: shunit2 $0" >&2
  shunit2 "$0" "$@"
fi