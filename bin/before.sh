#!/usr/bin/env sh
################################################################################
# shellcheck disable=SC1091
. "lib/utils.sh"
################################################################################

_debug "bin=${RPATH_BIN}, build=${RPATH_BUILD}, cache=${RPATH_CACHE}, coverage=${RPATH_COVERAGE}"
_debug "cache apt=${RPATH_CACHE_APT}, kcov=${RPATH_CACHE_KCOV}"
_debug "Installing runtime requirements: ${INSTALL}"
mkdir -pv "$RPATH_CACHE_APT" && _run_as_root apt-get -qq update
# shellcheck disable=SC2086
# _run_as_root apt-get -qq -o dir::cache::archives="$RPATH_CACHE_APT" --allow-unauthenticated install -y $INSTALL
_run_as_root apt-get -qq -o dir::cache::archives="$RPATH_CACHE_APT" install -y $INSTALL
test -d "${RPATH_CACHE_KCOV}" || git clone "$BUILD_KCOV_REPO" "${RPATH_CACHE_KCOV}"
_debug "Building kcov and having its dependencies installed ..."
_debug "Dependencies: ${BUILD_KCOV_WITH}"
# shellcheck disable=SC2086
# _run_as_root apt-get -qq -o dir::cache::archives="$RPATH_CACHE_APT" --allow-unauthenticated install -y ${BUILD_KCOV_WITH}
_run_as_root apt-get -qq -o dir::cache::archives="$RPATH_CACHE_APT" install -y $BUILD_KCOV_WITH
if ! mkdir -pv "$RPATH_BUILD_KCOV"; then
  _debug "Couldn't create build folder for kcov: '${RPATH_BUILD_KCOV}'"
  exit 1
fi