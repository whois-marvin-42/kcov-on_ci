# Purpose

This repo has been built to deal with an issue on GitLab CI ... **`kcov` is reporting coverage of 0.00% while it shouldn't** :angry: :rage: (with the default image used by GitLab CI runners)

Coverage is super low on purpose (but still ... it shouldn't be zero :smirk:) since it's done on a dummy script running a test with `shunit2` (guess what ... the one called `bin/dummy.sh` :wink: ... others are mainly to handle repeatability of the build process)

**CI configurations are far away from optimal, you shouldn't use them as a starting point!** They are only serving this debugging purpose. I tried to made them as simple/similar as possible only to allow easy comparison.

Doing so, CI platforms are sharing the provided scripts across all them  ... and, since I was able to build `kcov`, run tests and get a valid coverage with them, on all platforms, I'm looking for differences anywhere else, trying to figure what I am missing! Probably an undocumented requirement / library ?! :thinking:

# Status

## GitLab

### GitLab CI

> Coverage badge generated based on built-in coverage feature (regex)

| Branch      | Tests | Coverage  |
|---          |---    |---        |
| master      | [![pipeline status](https://gitlab.com/whois-marvin-42/kcov-on_ci/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/whois-marvin-42/kcov-on_ci/commits/master) | [![coverage report](https://gitlab.com/whois-marvin-42/kcov-on_ci/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/whois-marvin-42/kcov-on_ci/commits/master) |

## GitHub

### Travis CI

> Coverage will be sent to codecov.io (updated when TravisCI runner is executed)

| Branch      | Tests | Coverage  |
|---          |---    |---        |
| master      | [![Build Status](https://travis-ci.org/whois-marvin-42/kcov-on_ci.svg?branch=master)](https://travis-ci.org/whois-marvin-42/kcov-on_ci) | ![Not built-in](https://img.shields.io/badge/coverage-not%20built%20in-red.svg?style=for-the-badge&logo=travis) |

### Circle CI

> Coverage will be sent to codecov.io (updated when CircleCI runner is executed)

| Branch      | Tests | Coverage  |
|---          |---    |---        |
| master      | [![CircleCI](https://circleci.com/gh/whois-marvin-42/kcov-on_ci.svg?style=svg)](https://circleci.com/gh/whois-marvin-42/kcov-on_ci) | ![Not built-in](https://img.shields.io/badge/coverage-not%20built%20in-red.svg?style=for-the-badge&logo=circleci) |

### Coverage

| Branch      | codecov.io  |
|---          |---          |
| master      | [![codecov](https://codecov.io/gh/whois-marvin-42/kcov-on_ci/branch/master/graph/badge.svg)](https://codecov.io/gh/whois-marvin-42/kcov-on_ci) |
